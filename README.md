# Jaller's public notepad

A TiddlyWiki I use for non-confidential notes.

## Get it running locally

´´´bash
npm install --global tiddlywiki
tiddlywiki notes --listen port=9000
´´´

